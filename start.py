import pathlib
import click

from src import first_preparation_no_click
from src import creating_train_data_no_click


REPO_PATH = pathlib.Path.cwd()


@click.command()
def cli():
    first_preparation_no_click()
    creating_train_data_no_click(
        input_path_1=REPO_PATH / 'data/interim/osteo_fractures_01.csv',
        input_path_2=REPO_PATH / 'data/interim/osteo_fractures_02.csv',
        output_path=REPO_PATH / 'data/interim/fractures_concated.csv'
        )
    pass


if __name__ == '__main__':
    cli()
