import pandas as pd
import pathlib
import click


# функция убирает пробелы с краёв имён колонок df
def columns_strip(df: pd.DataFrame) -> None:
    df.columns = df.columns.str.strip()


# обрабатываем датасет
def df_preparation(xls: pd.io.excel._base.ExcelFile,
                   data_name: int | str,
                   limit: int,
                   output_path: pathlib.Path,
                   name_to_id: dict[str, int] | None = None,
                   is_df_2: bool = False) -> dict:
    df = pd.DataFrame(pd.read_excel(xls, data_name).iloc[:limit, 1:])
    # обработка имён колонок
    columns_strip(df)
    if is_df_2:
        # Два имени во 2 датасете написаны с ошибками, исправим
        df.replace(
            ['Чиркина Раиса Семновна', 'Шаробуеха Диана Сергеевна'],
            ['Чиркина Раиса Семеновна', 'Шаробуева Диана Сергеевна'],
            inplace=True
        )
    # convert index to column
    df.reset_index(inplace=True, names='id')
    if name_to_id is None:
        # формируем словарь перевода фамилии в id если его нет
        df['id'] = df['id']+1
        name_to_id = (df.iloc[:, [1, 0]].set_index('Ф.И.О.').to_dict())['id']

    # функция возвращает значение словаря или -1
    def return_name_value(
            name: str,
            name_to_id: dict[str, int] | None = name_to_id
            ) -> int:
        if name_to_id is None:
            return -1
        return name_to_id[name]

    # устанавливаем id в зависимости от имени
    df['id'] = (df['Ф.И.О.']).apply(return_name_value)
    # Сохраняем датасет без имён
    df.drop('Ф.И.О.', axis=1).to_csv(output_path, index=False)
    return name_to_id


def first_preparation_no_click() -> None:
    '''
    Ф-я обрабатывает входные данные .
    Так как в данных присутствуют имена пациентов меняем их на идентификаторы
    данные пришли в xlsx файле на шести страницах. Преобразуем их в 6
    датафреймов для удобства дальнейшей работы и сохраняем
    в папку data/processed
    '''
    # задаём относительные пути чтения -записи
    repo_path = pathlib.Path.cwd()
    raw_data_path = repo_path / 'data/raw/osteo_data.xlsx'
    processed_ata_path = repo_path / 'data/interim'
    # читаем
    xls = pd.ExcelFile(raw_data_path)
    # вытаскиваем имена листов
    data_names = xls.sheet_names
    # обработка первого датасета
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[0], limit=71,
        output_path=processed_ata_path / 'osteo_fractures_01.csv'
        )
    # обработка второго датасета
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[1], limit=71,
        output_path=processed_ata_path / 'concomitant_diseases_01.csv',
        name_to_id=name_to_id,
        is_df_2=True
        )
    # обработка третьего датасета Остео жалобы
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[2], limit=71,
        output_path=processed_ata_path / 'complaints_01.csv',
        name_to_id=name_to_id
        )
    # обработка четвёртого датасета
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[3], limit=52,
        output_path=processed_ata_path / 'osteo_fractures_02.csv'
        )
    # обработка пятого датасета
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[4], limit=52,
        output_path=processed_ata_path / 'concomitant_diseases_02.csv',
        name_to_id=name_to_id,
        )
    # обработка шестого датасета Остео жалобы
    name_to_id = df_preparation(
        xls=xls, data_name=data_names[5], limit=52,
        output_path=processed_ata_path / 'complaints_02.csv',
        name_to_id=name_to_id
        )
    pass


@click.command()
def first_preparation():
    first_preparation_no_click()
    pass


if __name__ == '__main__':
    first_preparation()
